DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_peminjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` int(15) NOT NULL,
  PRIMARY KEY (`id_detail_peminjam`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("10","1","56","1");
INSERT INTO detail_pinjam VALUES("11","46","57","1");
INSERT INTO detail_pinjam VALUES("12","46","58","1");
INSERT INTO detail_pinjam VALUES("13","1","59","2");
INSERT INTO detail_pinjam VALUES("14","1","60","2");
INSERT INTO detail_pinjam VALUES("15","1","61","1");
INSERT INTO detail_pinjam VALUES("16","1","62","1");
INSERT INTO detail_pinjam VALUES("17","45","63","1");
INSERT INTO detail_pinjam VALUES("18","1","64","1");
INSERT INTO detail_pinjam VALUES("19","46","65","1");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `jumlah` int(50) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tgl_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(50) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("1","Laptop","Baik","Hanya untuk area sekolah","16","1","2019-04-04 12:53:09","1","LAP","0");
INSERT INTO inventaris VALUES("45","Hardisk","Baik","-","12","1","2019-04-04 12:52:45","1","HD","0");
INSERT INTO inventaris VALUES("46","Headset","Baik","-","71","1","2019-04-04 12:54:50","2","HS","0");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `ket_jenis` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Elektronik","ELK-01","Alat Elektronik");
INSERT INTO jenis VALUES("2","Olahraga","OR-02","Alat Olahraga");
INSERT INTO jenis VALUES("3","Kesehatan","KES-03","Alat Kesehatan");
INSERT INTO jenis VALUES("4","ATK","ATK-01","Alat Tulis Kantor");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE login;

CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `session` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO login VALUES("1","admin","admin","admin","Luthfi Ramadhan");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Luthfi Ramadhan","2122000","Bogor");
INSERT INTO pegawai VALUES("2","Lorem ipsum","123","Lorem");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(50) NOT NULL,
  `tgl_pinjam` date DEFAULT NULL,
  `tgl_kembali` varchar(100) NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("56","KD020419","2019-04-02","","Sedang Dipinjam","1","1","1");
INSERT INTO peminjaman VALUES("57","KD020419","2019-04-02","02-04-2019","Sudah dikembalikan","2","1","46");
INSERT INTO peminjaman VALUES("58","KD020419","2019-04-02","02-04-2019","Sudah dikembalikan","2","1","46");
INSERT INTO peminjaman VALUES("59","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","1");
INSERT INTO peminjaman VALUES("60","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","2","1","1");
INSERT INTO peminjaman VALUES("61","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","1");
INSERT INTO peminjaman VALUES("62","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","1");
INSERT INTO peminjaman VALUES("63","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","45");
INSERT INTO peminjaman VALUES("64","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","1");
INSERT INTO peminjaman VALUES("65","KD040419","2019-04-04","04-04-2019","Sudah dikembalikan","1","1","46");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","e63d7f91a228c0fe93e823c6b95a6bc7","Admin","1");
INSERT INTO petugas VALUES("2","operator","4b583376b2767b923c3e1da60d10de59","Operator","2");
INSERT INTO petugas VALUES("3","user","ee11cbb19052e40b07aac0ca060c23ee","User","3");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `ket_ruang` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab 1 RPL","LAB-01","Untuk RPL");
INSERT INTO ruang VALUES("2","Lab 2 RPL","LAB-02","Untuk RPL");
INSERT INTO ruang VALUES("3","Bengkel","BK-01","Untuk Jurusan TKR ");



