<?php  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include 'inc/base.php';

if (isset($_POST['register'])) {
    $mail = new PHPMailer(true); // Passing `true` enables exceptions

	//CREATE
	$username = $_POST["username"];
	$password = $_POST["password"];
	$re_password = $_POST['re-password'];
	$email = $_POST["email"];
	$user_key = uniqid(md5(rndStr()));
	if ($password != $re_password) {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal, Password Tidak sama!',
		        'type' => 'error'
		    ];
	}

	$sqlCheck = "SELECT * FROM tm_anggota WHERE username = '$username' OR email = '$email'";
	$queryCheck = mysqli_query($conn,$sqlCheck);
	$row = mysqli_fetch_assoc($queryCheck);
	if ($row) {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal, Username / Email telah terpakai!',
		        'type' => 'error'
		    ];
	}
	if (empty($message)) {
		$password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "INSERT INTO tm_anggota(username,password,email,user_key) VALUES('$username','$password','$email','$user_key')";
		$query = mysqli_query($conn,$sql);
	    $emailTo = $email;
		if ($query) {
		    $id = mysqli_insert_id($conn);
		    $subject = '[RPL - BM3] Activation Account For: '.$username;
		    $deskripsi = 'Link Activation: https://myprojectku.xyz/sistem_login/verify.php?id='.$id.'&user_key='.$user_key;
		    //Server settings
		    $mail->SMTPDebug = 0; // Enable verbose debug output
		    $mail->isSMTP();   // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true; // Enable SMTP authentication
		    $mail->Username = '';  // SMTP username
		    $mail->Password = ''; // SMTP password
		    $mail->SMTPSecure = 'ssl';  // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465; // Ubah ke 587, bila anda menggunakan gmail

		    //Recipients
		    $mail->setFrom('no-reply@myprojectku.xyz', 'Activation Account');
		    $mail->addAddress($emailTo);     // Add a recipient
		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $deskripsi;

		    $mail->send();			
		    $message = [
		        'title' => 'Berhasil!',
		        'txt' => 'Registrasi Berhasil, silahkan cek email / folder spam anda untuk aktivasi akun!',
		        'type' => 'success'
		    ];
		}else{
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal!',
		        'type' => 'error'
		    ];
		}
	}
}
?>
<?php include ROOT.'inc/_header.php'; ?>

    <div class="container">
    	<div class="row justify-content-center align-items-center">
    		<div class="col-6">
    			<h1 class="display-3 text-center">Register</h1>		
    			<?php include ROOT.'inc/_message.php'; ?>
				<form method="post">
					<div class="form-group">
						<label>username</label>
						<input type="text" name="username" placeholder="Masukan username...." class="form-control" value="<?= isset($username) ? $username : '' ?>" required>
					</div> 
					<div class="form-group">
						<label>password</label>
						<input type="password" name="password" placeholder="Masukan password...." class="form-control" required>
					</div>
					<div class="form-group">
						<label>re-password</label>
						<input type="password" name="re-password" placeholder="Masukan re-password...." class="form-control" required>
					</div>					
					<div class="form-group">
						<label>email</label>
						<input type="email" name="email" placeholder="Masukan email...." class="form-control" value="<?= isset($email) ? $email : '' ?>" required>
					</div>
					<a href="login.php">Login</a>
					<button type="submit" name="register" class="btn btn-danger btn-block">Register</button>
				</form> 
    		</div>
		</div>    	
    </div>
<?php include ROOT.'inc/_footer.php'; ?>