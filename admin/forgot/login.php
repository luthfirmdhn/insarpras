<?php 
include 'inc/base.php';
if (isset($_POST['login'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];

	$sql = "SELECT * FROM tm_anggota WHERE username = '$username' OR email = '$username'";
	$query = mysqli_query($conn,$sql);
	$row = mysqli_fetch_assoc($query);

	if ($row) {
		$pass = $row['password']; // dari database
		$status = $row['status'];
		if (password_verify($password, $pass)) {
			if ($status == '0') { // Status user = 0
				$result = 'Akun anda belum diaktivasi!';
			}elseif($status == '1'){ // Kalo berhasil
				$result = 'Login berhasil';
			}else{
				$result = 'Status user tidak diketahi';
			}
		}else{
			$result = 'Password Salah!';
		}
	}else{
		$result = 'Username / Email tidak ada!';
	}


}
include ROOT.'inc/_header.php';
?>

    <div class="container">
    	<div class="row justify-content-center align-items-center">
    		<div class="col-6">
    			<h1 class="display-3 text-center">Login</h1>		
    			<?php if (isset($result)): ?>
				<div class="alert alert-primary" role="alert">
				 <?= $result ?>
				</div>    						
    			<?php endif ?>		
				<form method="post">
					<div class="form-group">
						<label>username or email</label>
						<input type="text" name="username" placeholder="Masukan username / email...." class="form-control">
					</div>
					<div class="form-group">
						<label>password</label>
						<input type="password" name="password" placeholder="Masukan password...." class="form-control">
					</div>
					<a href="forgot.php">Lupa Password ?</a>
					<a href="register.php">Register ?</a>
					<button type="submit" name="login" class="btn btn-danger btn-block">Register</button>
				</form>    			
    		</div>
		</div>    	
    </div>
<?php include ROOT.'inc/_footer.php'; ?>