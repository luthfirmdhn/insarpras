<?php
include "koneksi.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Data Jenis</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

</head>

<section class="content-header">
          <h1>
            Data Jenis Barang
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">
        <div class="tambah">
        <a href="index.php?page=cu_jenis"><button class="btn btn-success">Tambah Jenis Barang</button></a>
    </div></br>
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kode</th>
                                        <th>Keterangan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM jenis");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[nama_jenis]</td>
                                <td>$a[kode_jenis]</td>
                                <td>$a[ket_jenis]</td>
                                <td><a href=index.php?page=hapus&table=jenis&id=$a[id_jenis]><button class='btn btn-danger btn-circle'><i class='glyphicon glyphicon-trash'></i></button></a>
                                <a href=index.php?page=cu_jenis&table=jenis&id=$a[id_jenis]><button class='btn btn-info btn-circle'><i class='glyphicon glyphicon-pencil'></i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
        </section>
</html>