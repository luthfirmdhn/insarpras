<!DOCTYPE html>
<html>
<head>
    <title>Laporan</title>
</head>
<body>

<section class="content-header">
    <h2>Laporan</h2>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">
    <input type="button" value="Print" onclick="window.print()" id="cetak" class="no-print btn btn-primary pull-right">
    <a href="data/export_excel.php" type="button" class="btn btn-success pull-right no-print">Export Excel</a>
        <h4 align="center">Data Laporan Peminjaman</h4></br>
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Peminjaman</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Nama Peminjam</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status Peminjaman</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                            include "koneksi.php";
                                $data=mysql_query("SELECT * FROM peminjaman p JOIN pegawai s ON p.id_pegawai=s.id_pegawai JOIN inventaris i ON p.id_inventaris=i.id_inventaris RIGHT JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman WHERE status_peminjaman!='Sedang dipinjam' ORDER BY p.id_peminjaman DESC ");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[kode_peminjaman]</td>
                                <td>$a[nama]</td>
                                <td>$a[jumlah]</td>
                                <td>$a[nama_pegawai]</td>
                                <td>$a[tgl_pinjam]</td>
                                <td>$a[tgl_kembali]</td>
                                <td>$a[status_peminjaman]</td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
</section>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
</html>