  <?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Peminjaman</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

</head>

<section class="content-header">
          <h1>
            Peminjaman
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">

      <div class="col-md-6">
        <form action="" method="post">
          <div class="form-group">
            <input name="id_petugas" type="hidden" value="<?php echo $_SESSION['id_petugas'] ?>">
            <label>Nama Peminjam</label>
            <select name="id_pegawai" class="form-control" required>
              <option value="" selected disabled>- Nama Peminjam -</option>
              <?php
                $sql = mysql_query("SELECT * FROM pegawai");
                while ($data=mysql_fetch_array($sql)){
                  echo "<option value=$data[id_pegawai]> $data[nama_pegawai]</option>";
                }
              ?>
            </select><br></p>
            <label>Nama Barang</label>
            <select name="id_inventaris" class="form-control" required>
              <option value="" selected disabled>- Pilih Barang -</option>
              <?php
                $sql = mysql_query("SELECT * FROM inventaris");
                while ($data=mysql_fetch_array($sql)){
                  echo "<option value=$data[id_inventaris]> $data[nama]</option>";
                }
              ?>
            </select><br></p>
            <label>Jumlah</label>
            <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" required/><br></p>
            <input type="submit" name="pinjam" class="btn btn-success" value="Pinjam" />
            <input type="submit" name="cancel" class="btn btn-primary" value="Cancel" />
          </div>
        </form>
      </div>


<?php
  if(isset($_POST['cancel']))
    echo"<script>window.location.assign('index.php?page=peminjaman')</script>";
?>
  <?php

if(isset($_POST['pinjam']))
{

$id_petugas = $_SESSION['id_petugas'];
$id_inventaris = $_POST['id_inventaris'];
$id_pegawai = $_POST['id_pegawai'];
$kode_peminjaman = "KD".Date("dmy");
$jumlah = $_POST['jumlah'];
$tgl_pinjam = Date("Y-m-d");
$status_peminjaman = "Sedang Dipinjam";
$input = mysql_query("INSERT INTO peminjaman (tgl_pinjam,status_peminjaman,kode_peminjaman,id_petugas,id_pegawai,id_inventaris) VALUES('$tgl_pinjam','$status_peminjaman','$kode_peminjaman','$id_petugas','$id_pegawai','$id_inventaris')");
$dp = mysql_fetch_array(mysql_query("SELECT id_peminjaman FROM peminjaman ORDER BY id_peminjaman DESC LIMIT 1"));

$input2 = mysql_query("INSERT INTO detail_pinjam (id_inventaris,id_peminjaman,jumlah) VALUES ('$id_inventaris','$dp[id_peminjaman]','$jumlah')");
$sql = mysql_query("UPDATE inventaris SET jumlah= jumlah-$jumlah WHERE id_inventaris='$id_inventaris'");
  if($input){
    echo"<script>window.location.assign('index.php?page=peminjaman')</script>";
  }else{
    echo mysql_error();
  }
}
?>
</div>
</div>
</section>

<?php if($_SESSION['akses']=='admin' OR $_SESSION['akses']=='operator'){ ?>
<section class="content">
 <div class="box">
    <div class="box-body">
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Peminjaman</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Barang</th>
                                        <th>Nama Peminjam</th>
                                        <th>Tanggal Pinjam</th>
                                        
                                        <th>Status Peminjaman</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM peminjaman p JOIN pegawai s ON p.id_pegawai=s.id_pegawai JOIN inventaris i ON p.id_inventaris=i.id_inventaris JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman WHERE status_peminjaman='Sedang dipinjam' ORDER BY p.id_peminjaman DESC");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[kode_peminjaman]</td>
                                <td>$a[nama]</td>
                                <td>$a[jumlah]</td>
                                <td>$a[nama_pegawai]</td>
                                <td>$a[tgl_pinjam]</td>
                                
                                <td>$a[status_peminjaman]</td>
                                <td><a href=index.php?page=kembali&id_peminjaman=$a[id_peminjaman]&id_inventaris=$a[id_inventaris]&jumlah=$a[jumlah]><button class='btn btn-danger btn-circle'>Kembalikan</i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
</section>
<?php } ?>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
</html>