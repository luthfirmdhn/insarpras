<?php
include "koneksi.php";  
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Data Barang</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>

<section class="content-header">
          <h1>
            Data Barang <?php echo $_SESSION['nama_petugas'];?>
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">
    <div class="tambah">
        <a href="index.php?page=cu_inventaris"><button class="btn btn-success">Tambah Data Barang</button></a>
    </div></br>
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kondisi</th>
                                        <th>Keterangan</th>
                                        <th>Ruang</th>
                                        <th>Jumlah</th>
                                        <th>Jenis</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[nama]</td>
                                <td>$a[kondisi]</td>
                                <td>$a[keterangan]</td>
                                <td>$a[nama_ruang]</td>
                                <td>$a[jumlah]</td>
                                <td>$a[nama_jenis]</td>
                                <td><a href=index.php?page=hapus&table=inventaris&id=$a[id_inventaris]><button class='btn btn-danger btn-circle'><i class='glyphicon glyphicon-trash'></i></button></a>
                                <a href=index.php?page=cu_inventaris&table=inventaris&id=$a[id_inventaris]><button class='btn btn-info btn-circle'><i class='glyphicon glyphicon-pencil'></i></button></a> 
                                <a href=index.php?page=detail&table=inventaris&id=$a[id_inventaris]><button class='btn btn-warning btn-circle'><i class='glyphicon glyphicon-eye-open'></i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
</html>