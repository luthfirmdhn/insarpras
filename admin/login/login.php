<?php
	include "../koneksi.php";
	session_start();
	if(isset($_SESSION['nama'])){
		header("location:../index.php?page=dashboard");
	}else{
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Login</title>
    <link rel="icon" type="images/css" href="../../img/logo-insarpras.png">
     <!-- Bootstrap 3.3.4 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="style.css" rel="stylesheet">
  </head>
  <body>
    <div class="col-md-4 col-md-offset-4 form-login">
    

        <div class="outter-form-login">
        <div class="logo-login">
            <em class="glyphicon glyphicon-user"></em>
        </div>
            <form action="proses.php" class="inner-login" method="post">
            <h3 class="text-center title-login">Login Petugas</h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                
                <input type="submit" class="btn btn-block btn-custom-green" value="LOGIN" /></br>
                <a href="login_pegawai.php">Login sebagai pegawai</a>
                
                <div class="text-center forget">
                    <a href="../forgot/forgot.php">Forgot Password ?</a>
                </div>
                    <?php
                    /* handle error */
                    if (isset($_GET['pesan'])) {
                        if($_GET['pesan'] == "gagal"){
                            echo "<div class='alert alert-danger pesan-gagal' role='alert'>Username dan Password Salah!</div>";
                        }
                    }
                    ?>
            </form>
        </div>
    </div>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function validasi() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;       
        if (username != "" && password!="") {
            return true;
        }else{
            alert('Username dan Password harus di isi !');
            return false;
        }
    }
 
</script>
  </body>
</html>
<?php
}
?>